module.exports = {
  siteMetadata: {
    title: 'Gatsby Prismic Example',
    author: 'Batch Development',
    description: 'A starter blog demonstrating what Gatsby can do.',
    siteUrl: 'git@gitlab.com:maxshine/gatsby-prismic.git',
  },
  pathPrefix: '/gatsby-starter-blog',
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        //trackingId: `ADD YOUR TRACKING ID HERE`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Starter Blog`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/gatsby-icon.png`,
      },
    },
    'gatsby-plugin-offline',
    'gatsby-plugin-react-helmet',
		'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-typography',
      options: {
        pathToConfigModule: 'src/utils/typography',
      },
		},
		{
			resolve: "gatsby-source-prismic",
			options: {
				repositoryName: "maxtest001",
				accessToken: "MC5XNlIzSGlZQUFDWUFSYUVP.du-_ve-_ve-_vUJ9UXoR77-977-9UW8PO--_ve-_ve-_ve-_ve-_ve-_vUdITVbvv73vv71ET--_ve-_vTM",
				// See: https://prismic.io/docs/javascript/query-the-api/link-resolving
				linkResolver: ({ node, key, value }) => doc => {
					// Pretty URLs for known types
					if (doc.type === 'blog') return "/posts/" + doc.uid;
					if (doc.type === 'page') return "/" + doc.uid;
					// Fallback for other types, in case new custom types get created
					return "/doc/" + doc.id;
				},
				// See: https://prismic.io/docs/javascript/query-the-api/fetch-linked-document-fields
				fetchLinks: ['Color', 'Content Relationship', 'Date', 'Image', 'Key Text', 'Number', 'Rich Text', 'Select', 'Timestamp', 'Title'],
				// See: https://prismic.io/docs/nodejs/beyond-the-api/html-serializer
				htmlSerializer: ({ node, key, value }) => (
					(type, element, content, children) => {
						switch(type) {
							// case 'paragraph': return children = children.map(p => `<p class='paragraph'>${p}</p>`)
							default: return null
						}
					}
				),
			},
		},
  ],
}


// htmlSerializer: ({ node, key, value }) => (
// 	(type, element, content, children) => {
// 		switch(type) {
// 			// Add a class to paragraph elements
// 			case Elements.paragraph:
// 				return '<p class="paragraph-class">' + children.join('') + '</p>';
	 
// 			// Don't wrap images in a <p> tag
// 			case Elements.image:
// 				return '<img src="' + element.url + '" alt="' + element.alt + '">';
	 
// 			// Add a class to hyperlinks
// 			case Elements.hyperlink:
// 				var target = element.data.target ? 'target="' + element.data.target + '" rel="noopener"' : '';
// 				var linkUrl = PrismicDOM.Link.url(element.data, linkResolver);
// 				return '<a class="some-link"' + target + ' href="' + linkUrl + '">' + content + '</a>';
	 
// 			// Return null to stick with the default behavior for all other elements
// 			default:
// 				return null;
// 		}
// 	}
// ),