import React, { Component } from 'react'
import View from './View'

// export default () => {
// 	// const { name } = getCurrentUser()
// 	return (
// 		<View title="Your Profile">
// 			<p>Welcome back, { name && name }</p>
// 		</View>
// 	)
// }


export default class Admin extends Component {
	constructor(props) {
		super(props)
		this.state = { count: 0 }
		this.handleClick = this.handleClick.bind(this)
	}

	handleClick() {
		this.setState((state, props) => {
			return { count: state.count += 1 }
		})
	}

	render() {
		return (
			<View title="Your Profile">
				{ this.state.count }
				<button onClick={this.handleClick}>Click</button>
				<p>Welcome back, { name && name }</p>
			</View>
		)
	}
}