import React from "react"
import { Link } from 'gatsby'
import styles from './header.module.css'
import Nav from '../Nav'

export default ({ location }) => (
	<header className={styles.header}>
		<Nav location={location}/>
	</header>
)