import React from 'react'
import styles from './form.module.css'
import { navigate } from '@reach/router'

export default ({ handleSubmit, handleUpdate }) => (
	<form className={styles.form} method="post" onSubmit={event => {
		handleSubmit(event)
		navigate(`/admin/profile`)
	}}>
		<p className={styles[`form_instructions`]}>
			For this demo, please log in with the username <code>batch</code> and the password <code>admin</code>.
		</p>

		<label className={styles[`form__label`]}>
			Username
			<input type="text" className={styles[`form__input`]} name="username" onChange={handleUpdate} />
		</label>

		<label className={styles[`form__label`]}>
			Password
			<input type="password" className={styles[`form__input`]} name="password" onChange={handleUpdate} />
		</label>

		<input type="submit" value="Log In" className={styles[`form__button`]}/>
	</form>
)

