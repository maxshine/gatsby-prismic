import React, { Component } from 'react'
import { Link } from 'gatsby'
import styles from './nav.module.css'


export default class Nav extends Component {
	render() {
		const rootPath = `${__PATH_PREFIX__}/`
		const { location } = this.props
		return (
			<nav role="main" className={styles.nav}>
				{
					(function() {
						if (location.pathname === rootPath) {
							return (<Link to="/" className={styles[`nav__link`]}>Home</Link>)
						}
						return (<Link to="/" className={styles[`nav__link`]}>👈 Home</Link>)
					})()
				}
				<Link to="/app/home" className={styles[`nav__link`]}>Admin</Link>
				<Link to="/app/details" className={styles[`nav__link`]}>Details</Link>
				<Link to="/tags" className={styles[`nav__link`]}>Tags</Link>
				<Link to="/about-us" className={styles[`nav__link`]}>About Us</Link>
			</nav>

		)
	}
}
