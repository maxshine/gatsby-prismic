import React from 'react'
import Header from './Header'

import { rhythm } from '../utils/typography'

class Template extends React.Component {
  render() {
    const { location, children } = this.props
    return (
      <div
        style={{
          marginLeft: 'auto',
          marginRight: 'auto',
          maxWidth: rhythm(24),
          padding: `${rhythm(1.5)} ${rhythm(3 / 4)}`,
        }}
      >
				<Header location={location}/>
        {children}
      </div>
    )
  }
}

export default Template
