const isBrowser = typeof window !== 'undefined'

const getUser = () =>
	window.localStorage.gatsbyUser
		? JSON.parse(window.localStorage.gatsbyUser)
		: {}

const setUser = user => (window.localStorage.gatsbyUser = JSON.stringify(user))
