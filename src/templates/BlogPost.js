import React from 'react'
import Helmet from 'react-helmet'
import { Link,graphql } from 'gatsby'

import Layout from '../components/layout'
import { rhythm, scale } from '../utils/typography'

function renderTags (tagArr) {
	const tags = []
	tagArr.forEach(tagItem => {
		tagItem.new_tag.document.map(tag => tags.push(tag))
	})
	return tags.map((tag) => tag ? (<a style={{marginRight: '.5em'}} key="tag.uid" href={`/tags/${tag.uid}`} className="tag">{tag.data.title}</a>) : null)
}

class BlogPostTemplate extends React.Component {
  render() {
		const { pageContext, data, location } = this.props
		const { previous, next } = pageContext
		const { allPrismicPosts } = data
		const post = allPrismicPosts.edges[0].node
		// prismic custom field API_ID
		const {title, brief_content, publish_date, banner, content, tags} = post.data

    return (
      <Layout location={location}>
        <Helmet htmlAttributes={{ lang: 'en' }} meta={[{ name: 'description', content: brief_content.text }]} title={title.text}/>
        <h1>{title.text}</h1>
				<p style={{ ...scale(-1 / 5), display: 'block', marginBottom: rhythm(1), marginTop: rhythm(-1) }}>
					{publish_date} | {renderTags(tags)}
				</p>
				<div><img src={banner.url} alt={title.text} /></div>
        <div dangerouslySetInnerHTML={{ __html: content.html }} />

        <hr style={{ marginBottom: rhythm(1) }}/>

        <ul style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', listStyle: 'none', padding: 0 }}>
          <li>{previous && <Link to={previous.uid} rel="prev">← {previous.data.title.text}</Link>}</li>
          <li>{next && <Link to={next.uid} rel="next">{next.data.title.text} →</Link>}</li>
        </ul>
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
	query BlogPostById($prismicId: String) {
		allPrismicPosts(filter: { prismicId: { eq: $prismicId } }) {
			edges {
				node {
					prismicId
          data {
            title {
              text
            }
						publish_date
            brief_content {
              text
						}
						content {
							html
						}
						banner {
              url
						}

						tags {
							new_tag {
								document {
									uid
									data {
										title
									}
								}
							}
						}

						category_relationship {
							document {
								data {
									category
								}
							}
						}

          }
				}
			}
		}
	}
`