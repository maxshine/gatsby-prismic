import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { Link, graphql } from 'gatsby'

import Layout from '../components/layout'
import { rhythm, scale } from '../utils/typography'

// function PostListing() {

// }

class TagTemplate extends Component {
	render() {
		const { location, data, pageContext } = this.props
		const tagTitle = pageContext.title
		const { title, description } = data.site.siteMetadata
		// console.log(data.allPrismicPosts.edges)
		const posts = data.allPrismicPosts.edges

		return (
			<Layout location={location}>
				<Helmet htmlAttributes={{ lang: 'en' }} meta={[{ name: 'description', content: description.text }]} title={title.text} />
				<h2>Tag: {tagTitle}</h2>
				{posts.map(({node}) => {
          const postTitle = node.data.title.text
					return (
						<div key={node.uid}>
						  <h3 style={{ marginBottom: rhythm(1 / 4) }}><Link style={{ boxShadow: 'none' }} to={node.uid}>Title2: {postTitle}</Link></h3>
              <small>{node.data.publish_date}</small>
              <p>{node.data.brief_content.text}</p>
						</div>
					)
				})}
			</Layout>
		)
	}
}

export default TagTemplate

export const tag1Query = graphql`
	query BlogPostByTagUID($uid: String) {
		site {
			siteMetadata {
				title
				description
			}
		}
		allPrismicPosts(filter: {data: {tags: {elemMatch: {new_tag: {document: {elemMatch: {uid: {eq: $uid}}}}}}}}, sort: {fields: [data___publish_date], order: DESC}) {
			edges {
				node {
					uid
					data {
						title {
							text
						}
						publish_date
						brief_content {
							text
						}
					}
				}
			}
		}
	}	
`