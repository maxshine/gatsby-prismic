import React from 'react'
import { Link, graphql } from 'gatsby'
import get from 'lodash/get'
import castArray from 'lodash/get'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import { rhythm } from '../utils/typography'

function renderTags(tags) {
	const tagArr = []
	if (tags && tags[0] && tags[0].new_tag) {
		tags.forEach(tag => {
			tagArr.push(tag.new_tag.document[0])
		})
	}
	if (tagArr.length > 0) {
		return tagArr.map(tag => (<a style={{marginRight: '.3em'}} key={tag.uid} href={`/tags/${tag.uid}`}>{tag.data.title}</a>))
	}
	return null
}


export default class BlogIndex extends React.Component {
	constructor(props) {
		super(props)
	}
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    const siteDescription = get(this, 'props.data.site.siteMetadata.description')
    const posts = get(this, 'props.data.allPrismicPosts.edges')

    return (
      <Layout location={this.props.location}>
        <Helmet htmlAttributes={{ lang: 'en' }} meta={[{ name: 'description', content: siteDescription }]} title={siteTitle} />
        {posts.map(({node}) => {
					const { title, publish_date, brief_content, tags } = node.data
          return (	
            <div key={node.uid}>
              <h3 style={{ marginBottom: rhythm(1 / 4) }}><Link style={{ boxShadow: 'none' }} to={node.uid}>Post: {title.text}</Link></h3>
              <small>{publish_date} | {renderTags(tags)}</small>
              <p>{brief_content.text}</p>
            </div>
          )
				})}
      </Layout>
    )
  }
}

// TODO, how to do sorting in graphql
export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
    allPrismicPosts(sort: {fields: [data___publish_date], order: DESC}) {
      edges {
				node {
          prismicId
					uid
					type
          data {
            title {
              text
            }
            publish_date
            brief_content {
              text
						}

						tags {
							new_tag {
								document {
									uid
									data {
										title
									}
								}
							}
						}

          }
				}
			}
    }
  }
`
