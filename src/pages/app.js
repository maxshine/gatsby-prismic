import React from 'react'
import { Router } from '@reach/router'
import Admin from '../components/Admin'
import Layout from '../components/layout'

export default ({ location }) => (
	<Layout location={location}>
		<Router>
			<Admin path="/app/home"/>
		</Router>		
	</Layout>
)