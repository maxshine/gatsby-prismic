import React, { Component } from 'react'
import { Link, graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'
import { rhythm } from '../utils/typography'


export default class Tags extends Component {
	constructor(props) {
		super(props)
	}

  render() {
		const { location, data } = this.props
    return (
      <Layout location={location}>
				<Helmet
				htmlAttributes={{ lang: 'en' }}
				meta={[{ name: 'description', content: 'siteDescription' }]}
				title={'siteTitle'} />
				{data.allPrismicNewtag.edges.map(({node}) => (
					<div key={node.uid}>
						<h3 style={{ marginBottom: rhythm(1 / 4) }}><Link style={{ boxShadow: 'none' }} to={`/tags/${node.uid}`}>{node.data.title}</Link></h3>
					</div>
				))}
      </Layout>
    )
  }
}

export const tagsQuery = graphql`
  query {
    allPrismicNewtag {
			edges {
				node {
					uid
					data {
						title
					}
				}
			}
		}
  }
`
