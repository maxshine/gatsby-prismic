import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { Link, graphql } from 'gatsby'

import Layout from '../components/layout'
import { rhythm, scale } from '../utils/typography'

export default class AboutUsTemplate extends Component {
	render() {
		// GraphQL query insert as data object
		const { data, location } = this.props
		const aboutUs = data.allPrismicAboutUs.edges[0].node
		const {title, key_text, phone, content} = aboutUs.data
		const { siteMetadata } = data.site
		return (
			<Layout location={location}>
				<Helmet
					htmlAttributes={{ lang: 'en' }}
					meta={[{
						name: 'description',
						content: siteMetadata.description,
						title: `About Us - ${siteMetadata.title}`,
						author: siteMetadata.author,
						keywords: key_text,
					}]}/>
				<h1>{title.text}</h1>
				<div dangerouslySetInnerHTML={{ __html: content.html }}/>
				<hr/>
				<div>Contact Us: {phone}</div>
			</Layout>
		)
	}
}

export const aboutUsQuery = graphql`
	query AboutUs {
		site {
      siteMetadata {
        title
				author
				description
      }
		}
		allPrismicAboutUs {
			edges {
				node {
					uid
					data {
						key_text
						title { text }
						phone
						location {
							latitude
							longitude
						}
						content { html }
					}
				}
			}
		}
	}
`