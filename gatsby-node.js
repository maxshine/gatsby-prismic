const _ = require('lodash')
const Promise = require('bluebird')
const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')

exports.createPages = async ({ graphql, actions }) => {
	try {
		const { createPage } = actions
		const graphqlResult = await graphql(`
			{
				allPrismicNewtag {
					edges {
						node {
							uid
							data {
								title
							}
						}
					}
				}
				allPrismicPosts {
					edges {
						node {
							prismicId
							uid
							data {
								title {
									text
								}

								tags {
									new_tag {
										document {
											uid
											data {
												title
											}
										}
									}
								}

								category_relationship {
									document {
										data {
											category
										}
									}
								}

							}
						}
					}
				}
			}
		`)
		const BlogPostTemplate = path.resolve('./src/templates/BlogPost.js')
		const TagTemplate = path.resolve('./src/templates/Tag.js')

		const posts = graphqlResult.data.allPrismicPosts.edges // Array
		const tags = graphqlResult.data.allPrismicNewtag.edges // Array
		const postLength = posts.length
		// Creating Page - Posts
		posts.forEach((edge, index) => {
			const { uid, prismicId, data } = edge.node
			const previous = index === postLength - 1 ? null : posts[index + 1].node;
			const next = index === 0 ? null : posts[index - 1].node;
			// Creating Single Post Page
			createPage({
				path: uid,
				component: BlogPostTemplate,
				context: { prismicId, uid, previous, next }
			})
		})
		tags.forEach(({ node }) => {
			createPage({
				path: `tags/${node.uid}`,
				component: TagTemplate,
				context: {
					title: node.data.title,
					uid: node.uid,
				},
			})
		})

	} catch (error) {
		if (error) console.log(error)
		console.log('---------- ❌ Error @ gatsby-node.js ❌ ----------')
	}
}

exports.onCreatePage = async ({ page, actions }) => {
	const { createPage } = actions
	if (page.path.match(/^\/app/)) {
		// page.matchPath is a special key that's used for matching pages
		page.matchPath = `/app/*`
		// Update the page.
		createPage(page)
	}
}

/* END */
